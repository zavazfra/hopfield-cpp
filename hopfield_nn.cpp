#include <sstream>
#include "hopfield_nn.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


void printArray(vector<vector<int>> array) {
    for (size_t i = 0; i < array.size(); i++) {
        for (size_t j = 0; j < array[i].size(); j++) {
            cout << (array[i][j]>0?1:0);
            if((j+1) <array[i].size()) {
                cout << ',';
            }
        }
        cout << endl;
    }
}

void printArray(vector<vector<vector<int>>> array) {
    for (size_t i = 0; i < array.size(); i++) {
        printArray(array[i]);
        cout << endl;
    }
}

vector<vector<int>> * copyArray2D(vector<vector<int>> * array) {
    auto * copied = new vector<vector<int>>((*array).size(), vector<int>((*array)[0].size()));
    for(size_t y=0;y<(*array).size();y++) {
        for(size_t x=0;x<(*array)[0].size();x++) {
            (*copied)[y][x] = (*array)[y][x];
        }
    }
    return copied;
}

bool arraysSame(vector<vector<int>> * array1, vector<vector<int>> * array2) {
    for(size_t y=0;y<(*array1).size();y++) {
        for(size_t x=0;x<(*array1)[0].size();x++) {
            if((*array1)[y][x] != (*array2)[y][x]) {
                return false;
            }
        }
    }
    return true;
}

vector<vector<int>> * hopfield_nn::parseFileTo2D(string file) {
    ifstream input(file.data());
    string line;

    if (!input.is_open() || input.fail()) {
        cerr << "File is not able to open, file url:" << file << endl;
        exit(1);
    }

    auto * pattern = new vector<vector<int>>();  // 2D arrayTest
    while (getline(input, line)) {
        std::stringstream ss(line);
        auto num_line = vector<int>();
        string number;
        while (std::getline(ss, number, ';')) {
            if (number.empty()) {
                printf("Dont use ; on end of lines");
                break;
            }
            try {
                num_line.push_back(stoi(number)>0?1:-1);
            } catch (exception &e) {
                printf("String %s is not parsable to int", number.data());
                exit(1);
            }
        }
        pattern->push_back(num_line);
    }
    input.close();
    return pattern;
}

vector<vector<vector<int>>> *hopfield_nn::parseFileTo3D(string file) {
    // arrayLearn of 2D "images" (2D image is arrayLearn<arrayLearn<int>>)
    auto *array = new vector<vector<vector<int>>>();

    ifstream input(file.data());
    string line;

    if (!input.is_open() || input.fail()) {
        cerr << "File is not able to open, file url:" << file << endl;
        exit(1);
    }

    auto pattern = vector<vector<int>>();  // 2D arrayLearn

    while (getline(input, line)) {
        if (line.empty()) {
            array->push_back(pattern);
            pattern =  vector<vector<int>>();
            continue;
        }
        std::stringstream ss(line);
        auto num_line = vector<int>();
        string number;
        while (std::getline(ss, number, ';')) {
            if (number.empty()) {
                printf("Dont use ; on end of lines");
                break;
            }
            try {
                num_line.push_back(stoi(number)>0?1:-1);
            } catch (exception &e) {
                printf("String %s is not parsable to int", number.data());
                exit(1);
            }
        }
        pattern.push_back(num_line);
    }
    array->push_back(pattern);
    input.close();
    return array;
}

void hopfield_nn::calculateWeights() {
    size_t width = (*this->arrayLearn)[0][0].size();
    size_t height = (*this->arrayLearn)[0].size();
    size_t size = width * height;
    auto *weights_f = new vector<vector<int>>(size, vector<int>(size, 0));
    for (auto signArray : (*this->arrayLearn)) {
        for (size_t y = 0; y < size; y++) {
            for (size_t x = 0; x < size; x++) {
                if(x == y) continue;
                int posY1 = x/width;
                int posX1 = x%width;
                int posY2 = y/width;
                int posX2 = y%width;
                (*weights_f)[y][x] += (signArray[posY1][posX1]) * (signArray[posY2][posX2]);
            }
        }
    }
    this->weights = weights_f;
}

void hopfield_nn::learnFromFile(string path) {

    auto *array = this->parseFileTo3D(path);
    //printArray(*array);

    // testing array, width and height must be equals
    size_t height = (*array)[0].size();
    size_t width = (*array)[0][0].size();
    for (size_t i = 0; i < array->size(); i++) {
        if ((*array)[i].size() != height) {
            printf("Invalid size matrix of element %i, height", i);
            exit(1);
        }
        for (size_t j = 0; j < (*array)[i].size(); j++) {
            if ((*array)[i][j].size() != width) {
                printf("Invalid size matrix of element %i, line %i", i, j);
                exit(1);
            }
        }
    }
    this->arrayLearn = std::move(array);

    // calculate weights
    this->calculateWeights();
}

void hopfield_nn::testFromFile(string path) {
    auto *array = this->parseFileTo2D(path);
    //printArray(*array);
    size_t width = (*this->arrayLearn)[0][0].size();
    size_t height = (*this->arrayLearn)[0].size();
    if((*array).size() != height) {
        printf("Invalid size matrix of test, height");
        exit(1);
    } else {
        for(size_t i=0;i<(*array).size();i++) {
            if((*array)[i].size() != width) {
                printf("Invalid size matrix of test, width, line %i", i);
                exit(1);
            }
        }
    }
    this->arrayTest = array;
    auto * lastArrayState = new vector<vector<int>>(this->arrayTest->size(), vector<int>((*this->arrayTest)[0].size()));
    srand((unsigned)time(nullptr));
    while(!arraysSame(lastArrayState, this->arrayTest)) {
        delete lastArrayState;
        lastArrayState = copyArray2D(this->arrayTest);
        for(size_t i=0;i<100*width*height;i++) {
            this->randomChangePixel(rand());
        }
    }
    printf("I found result: \n");
    printArray(*this->arrayTest);
    delete lastArrayState;
}

void hopfield_nn::randomChangePixel(int random_d) {
    size_t width = (*this->arrayLearn)[0][0].size();
    size_t height = (*this->arrayLearn)[0].size();
    int random = random_d%(width*height);
    int randY = random/width;
    int randX = random%width;
    // vaha je brana jako cislo z pole weights[vstupnineuron][vystupnineuron]
    int finalNum = 0;
    for(size_t y=0;y<height;y++) {
        for(size_t x=0;x<width;x++) {
            finalNum += (*this->weights)[random][y*width+x]*(*this->arrayTest)[y][x];
        }
    }
    (*this->arrayTest)[randY][randX] = finalNum>=0?1:-1;
}

hopfield_nn::~hopfield_nn() {
    for(size_t i=0;i<(*this->weights).size();i++) {
        (*this->weights)[i].clear();
    }
    for(size_t i=0;i<(*this->arrayTest).size();i++) {
        (*this->arrayTest)[i].clear();
    }
    size_t wid = (*this->arrayLearn)[0].size();
    for(size_t i=0;i<(*this->arrayLearn).size();i++) {
        for(size_t f=0;f<wid;f++) {
            (*this->arrayLearn)[i][f].clear();
        }
        (*this->arrayLearn)[i].clear();
    }
    delete this->weights;
    delete this->arrayTest;
    delete this->arrayLearn;
}




