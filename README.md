# Hopfield Cpp

## Zadání
Aplikace používá neuronovou hopfieldovu síť pro zjištění obvykle symbolu, čísla, znaku nebo  vzoru. Data přijímá ze souborů uvedených v cmd argumentech. První soubor, defaultně pojmenovaný learn.txt obsahuje vzory, ze kterých se síť učí. Druhý soubor obsahuje jen jeden vzor, který se neuronová síť podle toho, co se naučila, snaží dostat. Aplikace běží na jednom vlákně.

Soubory jsou ve formátu matice, (MxN), binární (1 nebo 0), oddělené středníkem. Vzory jsou oddělené entrem (prázdným řádkem). 2D matice v programu je ```vector<vector<int>>```. Neurony zde nejsou reprezentovány vůbec, vše je uloženo pod jednou maticí (respektive dvěmi, jedna pro data, druhá pro váhy).

Výstup programu se vypíše do konzole. 

## CMD
Aplikace podporuje přepínač ```--help```, který vysvětlí, co aplikace umí
Aplikace má vzorová (defaultní) vstupní data, takže když se aplikace spustí bez dodatečných argumentů, pracuje s nima.