#ifndef SEMESTRALKA_HOPFIELD_NN_H
#define SEMESTRALKA_HOPFIELD_NN_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class hopfield_nn {
    private:
        vector<vector<vector<int>>> *arrayLearn;    // multiple 2D arrays
        vector<vector<int>> *arrayTest;             // single 2D array
        vector<vector<int>> *weights;
        void randomChangePixel(int);
        vector<vector<vector<int>>> * parseFileTo3D(string file);
        vector<vector<int>> * parseFileTo2D(string file);
        void calculateWeights();
    public:
        void learnFromFile(string path);
        void testFromFile(string path);
        ~hopfield_nn();
};


#endif //SEMESTRALKA_HOPFIELD_NN_H
