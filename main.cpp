#include <string>
#include "hopfield_nn.h"

using namespace std;


int main(int argc, const char* argv[])
{

	string learnFilePath = "learn.txt";
	string testingFilePath = "test.txt";

	// run.exe --learnFilePath learn.txt --testFilePath test.txt
	for (int i = 1; i < argc; ++i) {
		if (string(argv[i]) == "--learnFilePath") {
		    if(i+1 >= argc) {
		        printf("You must declare file path after switch --learnFilePath");
                exit(1);
		    }
			learnFilePath = argv[i+1];
		} else if(string(argv[i]) == "--testFilePath") {
            if(i+1 >= argc) {
                printf("You must declare file path after switch --testFilePath");
                exit(1);
            }
		    testingFilePath = argv[i+1];
        // if argv contains help
		} else if(string(argv[i]) == "--help") {
            // print help and exit
            printf("Usage: hopfields-nn.exe --learnFilePath learn.txt --testFilePath test.txt\n");
            printf("   or: hopfields-nn.exe --help\n\n");
            printf("Options:\n");
            printf("--learnFilePath: url to source file containing matrices of symbols to learn (default: learn.txt)\n");
            printf("--testFilePath:  url to source file containing matrix of texting symbol to determine (default: test.txt)\n");
            printf("--print          prints help\n");
            exit(0);
		} else if(string(argv[i]).rfind("--", 0) == 0) {
		    printf("Unknown param %s", argv[i]);
		    exit(1);
		}
	}

    ifstream learn(learnFilePath.c_str());
    ifstream testing(testingFilePath.c_str());
    if(learn.good() && testing.good()) {
        // application
        auto * test = new hopfield_nn();
        test->learnFromFile(learnFilePath);
        test->testFromFile(testingFilePath);
        delete test;
    } else {
        if(!learn.good()) {
            printf("File %s doesn't exist\n", learnFilePath.data());
        }
        if(!testing.good()) {
            printf("File %s doesn't exist\n", testingFilePath.data());
        }
    }

}